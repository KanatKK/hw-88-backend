const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const PostSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
    },
    time: {
      type: String,
    },
    author: {
        type: Object,
        required: true
    },
    image: {
        type: String,
    }
});

PostSchema.methods.generateAuthor = function (author) {
    this.author = author;
};

const Post = mongoose.model("Post", PostSchema);
module.exports = Post;