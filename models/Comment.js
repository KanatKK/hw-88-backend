const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const CommentSchema = new Schema({
    text: {
        type: String,
        required: true,
    },
    author: {
        type: Object,
        required: true,
    },
    postId: {
        type: String,
        required: true,
    },
});

CommentSchema.methods.generateAuthor = function (author) {
    this.author = author;
};

const Comment = mongoose.model("Comment", CommentSchema);
module.exports = Comment;