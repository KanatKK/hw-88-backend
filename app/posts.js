const router = require("express").Router();
const Post = require("../models/Post");
const User = require("../models/User");
const multer = require("multer");
const path = require("path");
const config = require("../config");
const {nanoid} = require("nanoid");
const auth = require("../middleware/auth");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});
const upload = multer({storage});

router.get("/", async (req, res) => {
    try {
        const users = await Post.find().sort({"time": 1});
        res.send(users);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get("/:id", async (req,res) => {
    const result = await Post.findById(req.params.id);
    if (result) {
        res.send(result);
    } else {
        res.sendStatus(404);
    }
});

router.post("/", upload.single("image"), auth, async (req, res) => {
    const postData = req.body;
    if (req.file) {
        postData.image = req.file.filename;
    }

    const post = new Post(postData);
    if (post.image === "" && post.description === "") {
        return res.status(400).send("Please, write description or upload image.");
    }
    try {
        post.generateAuthor({id: req.author._id, name: req.author.username});
        await post.save();
        res.send(post);
    } catch(e) {
        res.status(400).send(e);
    }
});

module.exports = router;