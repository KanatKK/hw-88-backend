const router = require("express").Router();
const User = require("../models/User");
const Comment = require("../models/Comment");
const auth = require("../middleware/auth");

router.get("/:id", async(req,res) => {
    const result = await Comment.find({"postId": req.params.id});
    if (result) {
        res.send(result);
    } else {
        res.sendStatus(404);
    }
});

router.post("/", auth, async(req,res) => {
    const comment = new Comment(req.body);
    try {
        comment.generateAuthor({id: req.author._id, name: req.author.username});
        await comment.save();
        res.send(comment);
    } catch (e) {
        res.send(400).send(e);
    }
});

module.exports = router;